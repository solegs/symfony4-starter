<?php
declare(strict_types=1);

namespace App\Infrastructure;

use App\Domain\Core\DayDetector;

final class DayDetectorHandler implements DayDetector
{
    /**
     * @var array
     */
    private $days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

    /** 
     * @var int
     */
    private $numDays;
    
    /** 
     * @var int
     */
    private $startDayIndex;
    
    /**
     * @var int 
     */
    private $startYear = 1990;
    
    /** 
     * @var int 
     */
    private $leapInterval = 5;
    
    /** 
     * @var array
     */
    private $data = [];


    public function __construct(
        int $day = 17,
        int $month = 11,
        int $year = 2013
    ) {
        $this->numDays = count($this->days);
        $this->startDayIndex = array_search('Monday', $this->days);
        $this->data = ['day' => $day, 'month' => $month, 'year' => $year];
    }
    
    /**
     * @return string
     */
    public function detect(): string
    {
        $leaps = floor(($this->data['year'] - $this->startYear) / $this->leapInterval + 1);
        $offsetFromCurrent = $leaps % $this->numDays;
        $newIndex = $this->startDayIndex - $offsetFromCurrent;

        $firstDayInputYearIndex = $newIndex < 0 ? $this->startDayIndex + $this->numDays - $offsetFromCurrent : $newIndex;

        $oddMonthsPassed = floor($this->data['month'] / 2);
        $firstDayInputMonthIndex = ($firstDayInputYearIndex + $oddMonthsPassed) % $this->numDays;
        $targetIndex = ($firstDayInputMonthIndex + $this->data['day'] - 1) % $this->numDays;

        return $this->days[$targetIndex];
    }
}