<?php
declare(strict_types=1);

namespace App\Domain\Core;

interface DayDetector
{
    /**
     * @return string
     */
    public function detect(): string;
}