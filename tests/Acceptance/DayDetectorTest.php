<?php
declare(strict_types=1);

namespace App\Tests\Acceptance;

class DayDetectorTest extends TestCase
{
    /**
     * @test
     */
    public function testShouldReturnSuccessDay()
    {
        $response = $this->request('GET', '/day-detector', [], []);
        $responseBody = json_decode($response->getContent(), true);

        $this->assertEquals(200, $response->getStatusCode());

        $this->assertEquals([
            'day' => 'Wednesday'
        ], $responseBody);
    }

    /**
     * @test
     */
    public function testShouldCompareWithWrongDay()
    {
        $response = $this->request('GET', '/day-detector', [], []);
        $responseBody = json_decode($response->getContent(), true);

        $this->assertEquals(200, $response->getStatusCode());

        $this->assertNotEquals([
            'day' => 'Sunday'
        ], $responseBody);
    }
}
