<?php
declare(strict_types=1);

namespace App\Controller;

use App\Services\DayDetectorService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as FOSRest;

/**
 * @Route("/")
 */
class DayDetectorController extends BaseController
{
    /**
     * @var DayDetectorService
     */
    private $dayDetectorService;

    /**
     * @param DayDetectorService $dayDetectorService
     */
    public function __construct(DayDetectorService $dayDetectorService)
    {
        $this->dayDetectorService = $dayDetectorService;
    }

    /**
     * @FOSRest\Get("/day-detector")
     *
     * @return Response
     */
    public function dayDetectorByDate(): Response
    {
        return $this->json(['day' => $this->dayDetectorService->getDayByDate()], Response::HTTP_OK);
    }
}