<?php
declare(strict_types=1);

namespace App\Services;

use App\Domain\Core\DayDetector;

class DayDetectorService
{
    /**
     * @var DayDetector
     */
    private $dayDetector;

    /**
     * @param  DayDetector $dayDetector
     */
    public function __construct(
        DayDetector $dayDetector
    ) {
        $this->dayDetector = $dayDetector;
    }

    /**
     * @return string
     */
    public function getDayByDate(): string
    {
        return $this->dayDetector->detect();
    }
}
