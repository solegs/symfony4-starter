<?php
declare(strict_types=1);

namespace App\Controller;

use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;

class BaseController
{
    /**
     * @param $data
     * @param int $status
     * @param array $headers
     * @param string[] $groups
     *
     * @return JsonResponse
     */
    protected function json($data, int $status = 200, array $headers = [], array $groups = []): JsonResponse
    {
        $headers = array_merge([
            'Content-Type' => 'application/json',
            'X-Items-Count' => count($data)
        ], $headers);

        $serializer = SerializerBuilder::create()->build();

        $serializationContext = SerializationContext::create()
            ->enableMaxDepthChecks()
            ->setGroups(array_merge(['Default'], $groups));

        $json = $serializer->serialize($data, 'json', $serializationContext);

        return new JsonResponse($json, $status, $headers, true);
    }
}