<?php
declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as FOSRest;

/**
 * @Route("/")
 */
class HeartBeatController extends BaseController
{
    /**
     * @FOSRest\Get("/heartbeat")
     *
     * @return Response
     */
    public function heartbeat(): Response
    {
        return $this->json(['message' => 'heartbeat!'], Response::HTTP_OK);
    }

}