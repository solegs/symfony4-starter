### Alias
```ini
alias dc="docker-compose"
alias o-composer="docker-compose exec php-fpm composer"
alias o-php="docker-compose exec php-fpm php"
alias o-phpunit="docker-compose exec php-fpm vendor/bin/phpunit"
```

## First deploy
```console
cp .env.dist .env
docker-compose pull
docker-compose up -d --build
o-composer install
```

## ACCESS
go to `http://localhost:8801/heartbeat`

### Testing
```ini
o-phpunit --stop-on-failure
```
